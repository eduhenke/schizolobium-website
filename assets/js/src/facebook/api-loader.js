window.fbAsyncInit = function() {
    FB.init({
        appId      : '1666633256716208',
        xfbml      : true,
        version    : 'v2.12'
    });
    FB.AppEvents.logPageView();
    $(document).ready(function(){
        var finished_rendering = function() {
            console.log("finished rendering plugins");
            var spinner = document.getElementById("spinner");
            spinner.removeAttribute("style");
            spinner.removeChild(spinner.childNodes[0]);
        }
        FB.Event.subscribe('xfbml.render', finished_rendering);
        AOS.init();
        checkLoggedIn();
        function checkLoggedIn(auth) {
            if (!FB.getAccessToken()) {
                showModal();
            }
        }
        function showModal() {
            $("#facebookModal").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
      
    })
};

(function(d, s, id) {
var js, fjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) {return;}
js = d.createElement(s); js.id = id;
js.src = "https://connect.facebook.net/pt_BR/sdk.js";
fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));