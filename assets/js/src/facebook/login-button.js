var app = angular.module('app');
app.directive('facebookLoginButton', ['$http', 'searchService', function($http, searchService){
    return {
        templateUrl: 'assets/js/src/facebook/login-button.html',
        link: function($scope) {
            window.afterLogin = function afterLogin() {
                console.log('Welcome! Fetching your information...');
                FB.getLoginStatus(handleLogin);
                function handleLogin(login){
                    if (!login.authResponse) {
                        console.log('Login canceled by user :(');
                        return;
                    }
                    console.log('Login info: ', login);
                    FB.api('/me', getPermissions.bind(null, login));
                }
                function getPermissions(login, user){
                    if (!user || !user.id) {
                        return console.error('No user data');
                    }
                    console.log('Good to see you, ', user.name);
                    console.log('User data: ', user);
                    searchService.user = user;
                    FB.api('/' + user.id + '/permissions', putUser.bind(null, login, user));
                }
                function putUser(login, user, permissions){
                    console.log('Permissions: ', permissions);
                    var grantedPermissions = _.filter(permissions.data, function(p){
                        return p.status === 'granted';
                    });
                    grantedPermissions = grantedPermissions.map(function(p){
                        return p.permission;
                    });
                    var data = {
                        grantedScopes: grantedPermissions.join(', ')
                    };
                    _.merge(data, login.authResponse, user);
                    $http.put("https://api.minhaconscienciajuridica.com.br/facebook/user", data).then(function(res){
                        console.log("put done! response: ", res);
                        $('#facebookModal').modal('hide');
                    }).catch(function(err){
                        console.error("put fail, http status: ", err.status, ", response: ", err);                                
                    })
                }
            }
        }
    }
}]);
