var app = angular.module('app', ['ngMask']);
console.log("loaded angular app");
app.service('searchService', function() {
    return {
        user: {},
        results: []
    }
})
app.controller("searchController", [ '$scope', '$http', 'searchService', function($scope, $http, searchService) {
    var controller = this;
    controller.valid = true;
    $scope.searchService = searchService;
    this.getData = function () {
        $http.put('https://api.minhaconscienciajuridica.com.br/cpf?userid=' + searchService.user.id, {
            number: searchService.user.cpf
        }).then(function(res){
            console.log(res);
            $http.get('https://api.minhaconscienciajuridica.com.br/processes', {
                params: {
                    cpf: clean(searchService.user.cpf),
                    userid: searchService.user.id
                }
            }).then(function(res){
                console.log(res);
                controller.valid = true;
                var processList = res.data.processes;
                _.each(processList, function(process, i){
                    processList[i] = _.omit(process, ['_id']);
                })
                controller.processList = processList;
            }).catch(function(err){
                controller.valid = false;
                console.error('Poxa eder, pelo menos isso tinha que acertar >:(');
            })
        }).catch(function(err){
            controller.valid = false;
            console.error(`User's a bad guy :(, trying to check other people's processes`);
        })
    }
    this.selectProcess = function (process) {
        this.selectedProcess = process;
    }
    this.unselect = function () {
        this.selectedProcess = null;
    }
    function clean(str){ // used for CPF
        return str.replace(/\./g,'').replace(/-/,'');
    }
}]);

app.filter('translate', function(){
    var translationMap = {
        ramoDireito: "Ramo do Direito",
        processo: "Número do Processo"
    };
    return function(field){
        return translationMap[field] || field;
    }
})
